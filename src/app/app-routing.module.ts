import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NewsAppDetailComponent } from './news-app-detail/news-app-detail.component';
import { NewsComponent } from './news/news.component';


const routes: Routes = [
  { path: 'news', component: NewsComponent },
  { path: 'news/:newsId', component: NewsAppDetailComponent },
  { path: '',   redirectTo: '/news', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
