import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewsAppDetailComponent } from './news-app-detail.component';

describe('NewsAppDetailComponent', () => {
  let component: NewsAppDetailComponent;
  let fixture: ComponentFixture<NewsAppDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewsAppDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewsAppDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
