import { Component, OnInit, Input } from '@angular/core';
import { NewsAddDetailService, NewsDetailRes, NewsComment } from '../service/news-add-detail.service';
import { DomSanitizer } from '@angular/platform-browser';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-news-app-detail',
  templateUrl: './news-app-detail.component.html',
  styleUrls: ['./news-app-detail.component.css']
})
export class NewsAppDetailComponent implements OnInit {


  newsId = null;

   commentObj: NewsComment;
   //{
  //   "userName": string,
  //   "comment": string,
  //   "vote": string,
  //   "postedTime": string
  // };


  newsDetail: NewsDetailRes;

  commentForm = this.fb.group({
    comment: [''],


  });

  constructor(private newsappDetailService: NewsAddDetailService, private sanitizer: DomSanitizer, private fb: FormBuilder, private activateRoute: ActivatedRoute) { }

  ngOnInit(): void {

    this.activateRoute.params.subscribe(params => this.newsId = params['newsId']);

    this.newsappDetailService.getNewsDetail(this.newsId).subscribe(res => this.newsDetail = res);
    this.commentObj = {
      "userName": "Selena",
      "comment": null,
      "vote": null,
      "postedTime": "new Date().getTime().toString()"
    };
  }

  sanitizedUrl(url: string) {
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }

  postComment() {

    if (this.commentForm.get('comment').value != "") {
      this.commentObj = {
        "userName": "Selena",
        "comment": this.commentForm.get('comment').value,
        "vote": "4",
        "postedTime": "2020-07-14T12:47:45Z"
      }
      this.newsDetail.comments.push(this.commentObj);
      this.commentForm.setValue({ 'comment': '' });
      this.newsappDetailService.patchComment(this.newsId, this.commentObj).subscribe(z => console.log(z));
    }
  }

}
