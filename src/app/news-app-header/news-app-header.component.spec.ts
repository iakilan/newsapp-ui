import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewsAppHeaderComponent } from './news-app-header.component';

describe('NewsAppHeaderComponent', () => {
  let component: NewsAppHeaderComponent;
  let fixture: ComponentFixture<NewsAppHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewsAppHeaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewsAppHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
