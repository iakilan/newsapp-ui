import { Component, OnInit, OnDestroy } from '@angular/core';
import { NewsAddDetailService } from '../service/news-add-detail.service';
import { Router } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';
import { NewsService, NewsRes } from '../service/news.service';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.css']
})
export class NewsComponent implements OnInit, OnDestroy {

  news: Array<NewsRes>;

  vimeoUrl = 'https://vimeo.com/197933516';
  youtubeUrl = 'https://www.youtube.com/watch?v=iHhcHTlGtRs';
  dailymotionUrl = 'https://www.dailymotion.com/video/x20qnej_red-bull-presents-wild-ride-bmx-mtb-dirt_sport';


  constructor(private newsappDetailService: NewsAddDetailService, private router: Router, private sanitizer: DomSanitizer, private newsService: NewsService) { }


  ngOnDestroy(): void {
    this.newsappDetailService.shareNewsDetail;
  }

  ngOnInit(): void {
    this.newsService.getNews().subscribe(res => this.news = res);

  }

  navigateDetailView(item: NewsRes) {
    this.newsappDetailService.shareNewsDetail = item;
    // this.router.navigateByUrl("/newsDetail")
    this.router.navigate(['/news', item.id]);
  }

  sanitizedUrl(url: string) {
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }

}
