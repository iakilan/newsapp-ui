import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';

export interface NewsDetailRes{
  "id":string,
  "url":string,
  "profileUrl":string,
  "content":string,
  "userName":string,
  "vote":string,
  "comments":Array<NewsComment>
}

export interface NewsComment{
  "userName":string,
    "comment":string,
    "vote":string,
    "postedTime":string
}

@Injectable({
  providedIn: 'root'
})
export class NewsAddDetailService {

  public shareNewsDetail;

  constructor(private http:HttpClient) { }

  getNewsDetail(newsId:string){
    let url = environment.baseUrl+"/"+newsId;

    return this.http.get<NewsDetailRes>(url);

  }

  patchComment(newsId:string, comment:NewsComment){
    let url = environment.baseUrl+"/"+newsId+"/comment";

    return this.http.patch<NewsDetailRes>(url,comment);

  }

}
