import { TestBed } from '@angular/core/testing';

import { NewsAddDetailService } from './news-add-detail.service';

describe('NewsAddDetailService', () => {
  let service: NewsAddDetailService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(NewsAddDetailService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
