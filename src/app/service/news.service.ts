import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

export interface NewsRes{
  id : string;
  url: string;
  profileUrl: string;
  name: string;
  content: string;
  vote: string;
  commentCount: string;

}


@Injectable({
  providedIn: 'root'
})
export class NewsService {

  

  constructor(private http:HttpClient) { }


  getNews(){
    return this.http.get<NewsRes[]>(environment.baseUrl);
  }
}
